<section class="layer">
    <div class="site-width">
        <h1>$Title</h1>
        <% with $Artist %>
            $Description
        <% end_with %>
    </div>
</section>
