<section class="layer">
    <div class="site-width">
        <h1>$Title</h1>
        $Content
        $Form
    </div>
</section>
