<?php

namespace {

    use SilverStripe\ORM\DataObject;
    use SilverStripe\Forms\TextField;
    use SilverStripe\Forms\HTMLEditor\HTMLEditorField;

    class Artist extends DataObject
    {
        private static $db = [
            'Name' => 'Varchar(255)',
            'Description' => 'HTMLText',
            'SortOrder' => 'Int'
    	];

        private static $many_many = [
            'Pages' => Page::class,
    	];

        private static $default_sort = 'ID DESC';

        private static $field_labels = [
    		'Title' => 'Titel',
    	];

        private static $summary_fields = [
        	'Name'
        ];
        
        public function getCMSFields()
        {
            $fields = parent::getCMSFields();

            $fields->removeByName([
                'SortOrder'
            ]);
    
            $fields->addFieldsToTab('Root.Main', [
                TextField::create('Name', 'Name'),
                HTMLEditorField::create('Description', 'Description')
            ]);
    
            return $fields;
        }

        public function Link() {
            $url = $this->Title;

            // Prep string with some basic normalization
            $url = strtolower($url);
            $url = strip_tags($url);
            $url = stripslashes($url);
            $url = html_entity_decode($url);
        
            // Remove quotes (can't, etc.)
            $url = str_replace('\'', '', $url);
        
            // Replace non-alpha numeric with hyphens
            $match = '/[^a-z0-9]+/';
            $replace = '-';
            $url = preg_replace($match, $replace, $url);
        
            $cleanUrl = trim($url, '-');
        
            // return $this->HomePage()->Link('artists/'.$this->ID.'-'.$cleanUrl);
            return ('/artists/'.$this->ID.'-'.$cleanUrl);
        }
    }
}
