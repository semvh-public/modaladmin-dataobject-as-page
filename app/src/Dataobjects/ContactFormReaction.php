<?php

namespace {

    use SilverStripe\ORM\DataObject;

    class ContactFormReaction extends DataObject
    {
        private static $db = [
        	'Name' => 'Varchar(255)',
        	'Email' => 'Varchar(255)',
        	'Phone' => 'Varchar(255)',
        	'Message' => 'HTMLText',
            'PrivacyCheck' => 'Boolean'
    	];

        private static $has_one = [
        	'ContactPage' => 'ContactPage'
    	];

        private static $default_sort = 'ID DESC';

        private static $field_labels = [
    		'Created' => 'Aangemaakt',
    		'Name' => 'Naam',
    		'Phone' => 'Telefoonnummer',
    		'Message' => 'Bericht',
    		'PrivacyCheck' => 'Akkoord met privacy',
    	];

        private static $summary_fields = [
        	'Created',
    		'Name'
    	];
    }
}
