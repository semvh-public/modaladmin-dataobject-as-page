<?php

namespace {
    use SilverStripe\CMS\Model\SiteTree;

    class NewsPage extends Page
    {
        private static $db = [
        ];

        private static $has_one = [];

        public function getCMSFields()
        {
            $fields = parent::getCMSFields();

            return $fields;
        }
    }
}
