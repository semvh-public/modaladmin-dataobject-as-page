<?php

namespace {
    use SilverStripe\CMS\Model\SiteTree;

    use SilverStripe\Forms\TextField;
    use SilverStripe\Forms\GridField\GridField;
    use SilverStripe\Forms\GridField\GridFieldConfig_RecordViewer;
    use SilverStripe\Forms\GridField\GridFieldConfig_RelationEditor;
    use UndefinedOffset\SortableGridField\Forms\GridFieldSortableRows;

    class ContactPage extends Page
    {
        private static $db = [
            'ContactSender' => 'Varchar(255)',
            'ContactReceiver' => 'Varchar(255)',
            'ContactSubject' => 'Varchar(255)'
        ];

        private static $has_one = [];

        private static $has_many = [
            'ContactFormReactions' => 'ContactFormReaction'
        ];

        public function getCMSFields()
        {
            $fields = parent::getCMSFields();

            // Form settings
            $fields->addFieldsToTab('Root.ContactFormSettings', [
                TextField::create('ContactSender', 'Sender'),
                TextField::create('ContactReceiver', 'Receiver'),
                TextField::create('ContactSubject', 'Subject')
            ]);

            // Form reactions
            $fields->addFieldsToTab('Root.ContactFormReactions', [
                GridField::create('ContactFormReactions', 'Reactions')
                    ->setList($this->ContactFormReactions())
                    ->setConfig(GridFieldConfig_RecordViewer::create())
            ]);

            return $fields;
        }
    }
}
