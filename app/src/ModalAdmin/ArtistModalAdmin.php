<?php

namespace {

    use SilverStripe\Admin\ModelAdmin;
    use SilverStripe\Forms\GridField\GridField;
    use UndefinedOffset\SortableGridField\Forms\GridFieldSortableRows;

    class ArtistAdmin extends ModelAdmin
    {
        private static $managed_models = [
            'Artist'
        ];

        private static $url_segment = 'artists';

        private static $menu_title = 'Artists';

        public function getEditForm($id = null, $fields = null)
        {
            $form = parent::getEditForm($id, $fields);

            // This check is simply to ensure you are on the managed model you want adjust accordingly
            if($this->modelClass == 'Artist' && $gridField = $form->Fields()->dataFieldByName($this->sanitiseClassName($this->modelClass))) {
                // This is just a precaution to ensure we got a GridField from dataFieldByName() which you should have
                if($gridField instanceof GridField) {
                    $gridField->getConfig()->addComponent(new GridFieldSortableRows('SortOrder'));
                }
            }

            return $form;
        }

    }
}
