<?php

namespace {

    use SilverStripe\CMS\Controllers\ContentController;
    use SilverStripe\Control\HTTPRequest;

    class ArtistsController extends ContentController
    {
        private static $allowed_actions = [
            'artists'
        ];

        public function artists(HTTPRequest $request) {
            $artist = Artist::get()->byID($request->param('ID'));
    
            if(!$artist) {
                return $this->httpError(404,'That artist could not be found');
            }
    
            return array (
                'Artist' => $artist,
                'Title' => $artist->Title
            );
        }

        protected function init()
        {
            parent::init();
        }
    }
}
