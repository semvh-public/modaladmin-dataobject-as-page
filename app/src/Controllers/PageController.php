<?php

namespace {

    use SilverStripe\CMS\Controllers\ContentController;
    use SilverStripe\View\Requirements;
    use SilverStripe\Control\HTTPRequest;

    class PageController extends ContentController
    {
        private static $allowed_actions = [
            'artists'
        ];

        public function artists(HTTPRequest $request) {
            $artist = Artist::get()->byID($request->param('ID'));
    
            if(!$artist) {
                return $this->httpError(404,'That artist could not be found');
            }
    
            return array (
                'Artist' => $artist,
                'Title' => $artist->Title
            );
        }

        protected function init()
        {
            parent::init();
            // You can include any CSS or JS required by your project here.
            // See: https://docs.silverstripe.org/en/developer_guides/templates/requirements/

            // requirements
            Requirements::themedJavascript('js/plugin/svg4everybody/svg4everybody.min.js');
            Requirements::themedJavascript('js/plugin/jquery/jquery-3.3.1.min.js');
            Requirements::themedJavascript('js/script.min.js');
        }
    }
}
