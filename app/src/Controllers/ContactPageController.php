<?php

namespace {

    use SilverStripe\Forms\Form;
    use SilverStripe\Forms\FieldList;
    use SilverStripe\Forms\HiddenField;
    use SilverStripe\Forms\TextField;
    use SilverStripe\Forms\TextareaField;
    use SilverStripe\Forms\EmailField;
    use SilverStripe\Forms\NumericField;
    use SilverStripe\Forms\CheckboxField;
    use SilverStripe\Forms\literalField;
    use SilverStripe\Forms\FormAction;
    use SilverStripe\Control\Email\Email;
    use SilverStripe\Forms\RequiredFields;

    class ContactPageController extends PageController {

        private static $allowed_actions = ['Form', 'ThankYou'];

        private static $url_handlers = [
            'bedankt' => 'ThankYou',
            'thank-you' => 'ThankYou'
        ];

        protected function init()
    	{
    		parent::init();
    	}

        public function Form()
        {
            $fields = new FieldList(
                // Anti spam
			    HiddenField::create('AngryCat2'),
			    TextField::create('NameName')
                    ->addExtraClass('visuallyhidden')
                    ->setAttribute('tabindex', '-1'),

                // Form fields
                TextField::create('Name', _t("Forms.NAME","Naam"))
                    ->addExtraClass('half'),
                EmailField::create('Email', _t("Forms.EMAIL","E-mail"))
                    ->addExtraClass('half'),
                NumericField::create('Phone', _t("Forms.PHONE","Telefoonnummer"))
                    ->addExtraClass('half half--last'),
                TextareaField::create('Message', _t("Forms.MESSAGE","Bericht")),
                CheckboxField::create('PrivacyCheck')
                    ->setTitle(
                        literalField::create("", _t("Forms.PRIVACY_LABEL","Met het versturen van je gegevens ga je akkoord met de wijze waarop wij je gegevens verwerken (zie <a href='/privacy' target='_blank'>privacy statement</a>)."))
                    )
            );

            $actions = new FieldList(
                new FormAction('submit', _t("Forms.SUBMIT","Verstuur"))
            );

            // Required fields
            $validator = new RequiredFields('Name', 'Email', 'Message', 'PrivacyCheck');

            return new Form($this, 'Form', $fields, $actions, $validator);
        }

        public function submit($data, $form)
        {
            if(strlen($data['AngryCat2']) < 1 && strlen($data['NameName']) < 1) {
                $reaction = ContactFormReaction::create();

                $form->saveInto($reaction);
                $reaction->ContactPageID = $this->ID;

                $reaction->write();

                Email::create()
                    ->setFrom($this->ContactSender)
                    ->setTo($this->ContactReceiver)
                    ->setSubject($this->ContactSubject)
                    ->setHTMLTemplate('Email\\ContactEmail')
                    ->setData([
                        'Reaction' => $reaction
                    ])
                    ->send();

                $this->redirect($this->Link()._t("Url.THANK_YOU", "thank-you"));
            } else {
                return $this->customise(array( 'Content' => 'Je bericht werd gedetecteerd als spam. Probeer het nog eens en zorg dat alle velden juist ingevuld zijn.', 'Form' => ''))->renderWith(array('Page'));
            }
        }

        public function ThankYou()
        {
           return $this->renderWith(array('ContactPage_ThankYou', 'Page'));
        }
    }
}
