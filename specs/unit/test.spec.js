import App from '../../src/public/js/app.js';

describe("App", function () {

  // Test data
  var app;

  // Called before every test
  beforeEach(function() {
    app = new App();
  });

  // Check the element for the proper data
  // Other data cannot be tested as it involves DOM elements
  it("should exist", function () {
    expect(app).toBeDefined();
  });
});
